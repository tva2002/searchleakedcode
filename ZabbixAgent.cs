﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using SearchLeakedCode.Models;
using ZabbixSender;
using System.Threading;

namespace SearchLeakedCode
{
    class ZabbixAgent
    {
        private readonly ILogger _logger;

        public ZabbixAgent(Serilog.ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }

        public void Send(string msg)
        {
            Thread.Sleep(10000);
            try
            {
                Config conf = new Config();
                GetConfig[] zabbixCfg = conf.GetZabbixCfg();
                
                string zbxServer = zabbixCfg[0].ZbServer;
                int zbxServerPort = zabbixCfg[0].ZbServerPort;
                int zbxServerTimeOut = zabbixCfg[0].ZbServerTimeOut;
                string zbxHost = zabbixCfg[0].ZbHost;
                string zbxKey = zabbixCfg[0].ZabbixKey;
                
                ZabbixSenderRequest zabbixAgent = new ZabbixSenderRequest(zabbixCfg[0].ZbHost, zabbixCfg[0].ZabbixKey, msg);
                _logger.Debug("Send message for zabbix server is " + msg);
                string result = zabbixAgent.Send(zabbixCfg[0].ZbServer, zabbixCfg[0].ZbServerPort, zabbixCfg[0].ZbServerTimeOut).Response;
                
                if (result == "success")
                {
                    _logger.Debug("Response from zabbix server is " + result);
                }
                else
                {
                    _logger.Error("Response from zabbix server is " + result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Zabbix alert send error: {0}", ex);
            }
        }

    }
}
