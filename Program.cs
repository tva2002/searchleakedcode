﻿using RestSharp;
using SearchLeakedCode.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using SearchLeakedCode.Data;
using Serilog;
using System.Threading;

namespace SearchLeakedCode
{
    class Program
    {
         public static XmlData count = new XmlData();
         public static int mark = 1;

        public static string GetSearchLeakedCode(int cout, string repofaunds,string strSearch)
        {

            ZabbixAgent zabbixAgent = new ZabbixAgent(Log.Logger);

            string urlSearchCode = "https://searchcode.com/api/codesearch_I/";


            var client = new RestClient(baseUrl: urlSearchCode);

            var request = new RestRequest();
            request.AddQueryParameter(name: "q", value: strSearch);
            request.AddQueryParameter(name: "src", value: "5");
            request.AddQueryParameter(name: "src", value: "3");
            request.AddQueryParameter(name: "src", value: "2");
            request.AddQueryParameter(name: "src", value: "1");

            IRestResponse response = client.Execute(request);


            if (response.StatusCode == HttpStatusCode.OK)
            {
                string findRepo = "";
                List<string> findRepoAllData = new List<string>();
                var rezult = JsonSerializer.Deserialize<CodeSearchRezult>(response.Content);
                Console.WriteLine("Total: " + rezult.total.ToString());
                   foreach (var source in rezult.results)
                   {
                      if (findRepo != "" && source.repo.ToString() !="")
                          { findRepo += ";" + source.repo.ToString(); }
                      else
                          { findRepo += source.repo.ToString(); }
                      findRepoAllData.Add( "Repo: " + source.repo.ToString() + " --Soure: " + source.url.ToString() + " file: " + source.filename.ToString());
 
                }
                if (rezult.total != cout && rezult.total != 0)
                {

                    if (findRepo != repofaunds)
                    {
                        string foundResult = "";
                        Log.Debug("Searching string is " + strSearch);
                        foreach (string debugString in findRepoAllData)
                        {
                            foundResult += "Found is " + debugString+" || ";
                            Log.Debug("Found is " + debugString);
                        }


                        //zabbixAgent.Send("https://searchcode.com/api/codesearch_I/?q=" + strSearch + "&src=5&src=3&src=2&src=1");
                        zabbixAgent.Send("Searching string is " + strSearch + " : "+ foundResult);
                        mark = mark * 0;
                    }
                    else
                    {
                        Log.Debug("Searching string is " + strSearch);
                        foreach (string debugString in findRepoAllData)
                        {
                            Log.Debug("Found is " + debugString);
                        }
                    }
                }
                mark = mark * 1;
                  return rezult.total.ToString()+"="+ findRepo;
            }

            mark = mark * 1;
            return "-1";
        }

        static void Main(string[] args)
        {
           List<string> data = new List<string>();

           Log.Logger = new LoggerConfiguration()
              .MinimumLevel.Debug()
              .WriteTo.Console()
              .WriteTo.File("/var/log/SearchLeakedCode.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 5)
              .CreateLogger();
            Config conf = new Config();
            ZabbixAgent zabbixAgent = new ZabbixAgent(Log.Logger);
            var strinsForSearch = conf.GetSearchStr();
            
            var counts = count.GetAllCountInRepo().ToArray();

            int n = 0;
            
            foreach (string strSearch in strinsForSearch)
            {
                string[] strData = counts[n].Split('=');
                string result = GetSearchLeakedCode(int.Parse(strData[0]), strData[1], strSearch);
                if (result != "-1")
                    data.Add(result);
                    Thread.Sleep(10000);
                n=n+1;
            }
            count.AddDataInXml(data.ToArray());
            if (mark == 1)
            {
               
                zabbixAgent.Send("OK");
               
            }

           // Console.ReadKey();

        }
            
    }
}

