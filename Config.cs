﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SearchLeakedCode.Models;
using Serilog;


namespace SearchLeakedCode
{
    class Config
    {
        public GetConfig[] GetZabbixCfg()
        {
            try
            {

                string[] Lout= { };
                XDocument xdoc = XDocument.Load("zabbixconf.xml");
                var items = from xe in xdoc.Element("zabbix").Elements("zabbixcfg")
                            select new GetConfig
                            {
                                ZbServer = xe.Element("ZabbixServer").Value,
                                ZbServerPort = Convert.ToInt32( xe.Element("ZabbixServerPort").Value),
                                ZbServerTimeOut = Convert.ToInt32(xe.Element("ZabbixServerTimeOut").Value),
                                ZbHost = xe.Element("ZabbixHost").Value,
                                ZabbixKey = xe.Element("ZabbixKey").Value
                            };
                var d = items.ToArray();
                
                return d; 
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("zabbix agent confiration error: [{0}].", ex.ToString()), ex);
                return null;
            }

        }

        public List<string> GetSearchStr()
        {
            try
            {

                List<string> Lout = new List<string>() { };
                XDocument xdoc = XDocument.Load("config.xml");
                var items = from xe in xdoc.Element("CodeStrings").Elements("CodeString")
                            select new GetConfig
                            {
                                
                                STR = xe.Element("String").Value
                                
                            };
                foreach (var item in items)
                    Lout.Add( item.STR.ToString() );
                return Lout;
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Process confiration error: [{0}].", ex.ToString()), ex);
                return null;
            }

        }


    }
}
