﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchLeakedCode.Models
{
    class XmlDataModel
    {       
        public string filename { get; set; }
        public string countInRepo { get; set; }
        public string repo { get; set; }
        public string url { get; set; }
    }
}
