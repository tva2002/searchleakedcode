﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchLeakedCode.Models
{
    class CodeSearchRezult
    {
        public string matchterm { get; set; }
        public int previouspage { get; set; }
        public string searchterm { get; set; }
        public string query { get; set; }
        public int total { get; set; }
        public int page { get; set; }
        public int nextpage { get; set; }
        public List<RezultResponce> results { get; set; }
        public List<LanguageFilters> language_filters { get; set; }
        public List<SourceFilters> source_filters { get; set; }        
    }
}
