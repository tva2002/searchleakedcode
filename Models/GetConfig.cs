﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchLeakedCode.Models
{
    class GetConfig
    {
        public int ID { get; set; }
        public string STR { get; set; }
        public string ZbServer { get; set; }
        public int ZbServerPort { get; set; }
        public int ZbServerTimeOut { get; set; }
        public string ZbHost { get; set; }
        public string ZabbixKey { get; set; }

    }
}
