﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using SearchLeakedCode.Models;
using Serilog;

namespace SearchLeakedCode.Data
{
    class XmlData
    {
        public List<string> GetAllCountInRepo()
        {
            try
            {
                string dataFileName = "CodeSearch.xml";
                if (File.Exists(dataFileName))
                {
                    List<string> Lout = new List<string>() { };
                    XDocument xdoc = XDocument.Load(dataFileName);
                    var items = from xe in xdoc.Element("repos").Elements("totalrow")
                                select new XmlDataModel
                                {
                                    countInRepo = xe.Element("count").Value,
                                };
                    foreach (var item in items)
                        Lout.Add(item.countInRepo);
                    return Lout;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Error getting count from xml file: [{0}].", ex.ToString()), ex);
                return null;
            }
        }

        public void SerializeDataSet(List<string> dat)
        {
            // Creates a DataSet for user info
            string[] strInXml;
            try
            {
                DataTable table_repository = new DataTable("repos");
                table_repository.Columns.Add("repo", typeof(string));
                table_repository.Columns.Add("url", typeof(string));
                table_repository.Columns.Add("filename", typeof(string));
                // adds table
                //vault.Tables.Add(table_users);
                foreach (string mydat in dat)
                {
                    strInXml = mydat.Split(";");
                    table_repository.Rows.Add(new object[] { strInXml[0], strInXml[1], strInXml[2] });
                }
                // serialized table
                string filenames = DateTime.Now+"Resources.xml";
                table_repository.WriteXml(filenames);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Error writing to file: [{0}].", ex.ToString()), ex);
            }
        }

        public String AddDataInXml(string[] date)
        {
            XDocument xdoc = XDocument.Load("CodeSearch.xml");
            XElement root = xdoc.Element("repos");


            foreach (XElement xe in root.Elements("totalrow").ToList())
            {
                xe.Remove();
            }

            foreach (string dat in date)
            {
                
                root.Add(new XElement("totalrow", new XElement("count", dat.ToString())));
            }
            xdoc.Save("CodeSearch.xml");
            return xdoc.ToString();
        }

        public string EditDataInXml(int date)
        {
            try
            {
                XDocument xdoc = XDocument.Load("CodeSearch.xml");
                XElement root = xdoc.Element("repos");

                foreach (XElement xe in root.Elements("totalrow").ToList())
                {       
                        xe.Element("count").Value = date.ToString();                    
                }

                xdoc.Save("CodeSearch.xml");
                return "OK";
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Error writing to file: [{0}].", ex.ToString()), ex);
                return null;
            }
        }

    }
}
